import "./Menu.css";
import React from "react";
import Home from '../Home/Home'
import Product from '../Products/Products'
import {HashRouter,Route} from 'react-router-dom'

function template() {
  return (
    <div className="menu">
      <HashRouter>
        <div className="menu-items">
      <a href="#/home" className="alert-link">Home</a>
      <a href="#/Products"  className="alert-link">Products</a>
      </div>
      <div>
      <Route path='/' exact component={Home}></Route>
      <Route path='/home' component={Home}></Route>
      <Route path='/Products' component={Product}></Route>
      </div>
      </HashRouter>
    </div>
    

  );
};

export default template;
