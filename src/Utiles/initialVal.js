import shoe from '../images/shoe.jpeg'
import suitcase from '../images/suitcase.jpeg'
import watch from '../images/watch.jpeg'
const initialVal = {
    currency:'INR',
    data: [
        {
            "p_name": "Shoe",
            "p_image": shoe,
            "p_cost": 535
        },
        {
            "p_name": "Suitcase",
            "p_image": suitcase,
            "p_cost": 994
        },
        {
            "p_name": "Watch",
            "p_image": watch,
            "p_cost": 1224
        }
    ]
}
export default initialVal;