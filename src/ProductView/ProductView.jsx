import "./ProductView.css";
import React from "react";

function template() {
  const {p_name,p_image,p_cost} =this.props.product
  return (
    <div className="product-view">
         <div><img src={p_image} width="250" height="250px;" ></img></div>
         <div>{p_name}</div>
         <div>{p_cost} <span>{this.props.currency == 'INR' ? <span>&#x20B9;</span> : <span>&#36;</span>}</span></div>
    </div>
  );
};

export default template;
