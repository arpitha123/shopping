import React    from "react";
import template from "./ProductView.jsx";
import {connect} from 'react-redux'

class ProductView extends React.Component {
  render() {
    return template.call(this);
  }
}
const mapStateToProps=(state)=>{
  return {
    currency:state.shoppingReducer.currency
  }
}
export default connect(mapStateToProps)(ProductView);

