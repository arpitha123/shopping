import axios from 'axios';
const baseUrl='https://api.exchangeratesapi.io/'
class ServerCall{
   static fnGetReq(url){
       return axios.get(baseUrl+url);
   }
}

export default ServerCall;