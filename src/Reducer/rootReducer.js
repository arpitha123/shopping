import {combineReducers} from 'redux'
import shoppingReducer from '../Reducer/shoppingReducer'
const rootReducer=combineReducers({
    shoppingReducer
})
export default rootReducer;