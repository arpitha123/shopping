import {call,put,takeLatest} from 'redux-saga/effects'
import ServerCall from '../services/ServerCall';
function* fnGetData(data){
    const res=yield ServerCall.fnGetReq('latest?base='+data.curr);
    debugger;
    let currVal=res.data.rates[data.prevCurr];
    let singleVal=1/currVal;
    let updatedData=data.currData.map((obj)=>{
          obj.p_cost=Math.round(obj.p_cost*singleVal);
          return obj;
    })

    yield put({
        type:'UPDATE',
        data:updatedData,
        currency:data.curr
    })

}
function* shoppingSaga(){
yield takeLatest("CHANGE_CURR",fnGetData)
}
export default shoppingSaga;