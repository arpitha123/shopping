import {all} from 'redux-saga/effects'
import shoppingSaga from './shoppingSaga'
function* rootSaga(){
    yield all([shoppingSaga()])
}
export default rootSaga;