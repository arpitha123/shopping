import React    from "react";
import template from "./Products.jsx";
import {connect} from 'react-redux'
class Products extends React.Component {
  render() {
    return template.call(this);
  }
  fnCurrChange(eve){
     let curr=eve.target.value;
     this.props.dispatch({
       type:'CHANGE_CURR',
       curr:curr,
       prevCurr:this.props.prevCurr,
       currData:this.props.data
     })
  }
}
const mapDispatchToProps=(dispatch)=>{
  return {
    dispatch
  }
}
const mapStateToProps=(state)=>{
  return {
    data:state.shoppingReducer.data,
    prevCurr:state.shoppingReducer.currency
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Products);
