import "./Products.css";
import React from "react";
import ProductView from '../ProductView/index'
function template() {
  const { data } = this.props;
  return (
    <div className="products container-fluid">
      <div className="row">
        <div className='col-sm-9'>
          {
            data.map((obj, i) => {
              return <ProductView key={i} product={obj} />
            })
          }
        </div>
        <div className="col-sm-1">
          Currency:
        </div>
        <div className="col-sm-2">
          <select className="form-control" onChange={this.fnCurrChange.bind(this)}>
            <option>INR</option>
            <option>USD</option>
          </select>
        </div>
      </div>
    


    </div>
  );
};

export default template;
